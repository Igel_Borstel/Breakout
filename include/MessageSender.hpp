#ifndef INCLUDE_MESSAGESENDER_HPP
#define INCLUDE_MESSAGESENDER_HPP

#include <map>
#include <string>
#include <memory>
#include "Listener.hpp"
#include "messages/Message.hpp"

namespace breakout
{
	class MessageSender
	{
		std::multimap<std::string, Listener*> listenerMap;
		std::multimap<Listener*, std::multimap<std::string, Listener*>::iterator> listenerAddressMap;
	public:
		void registerListener(Listener*, std::unique_ptr<Message>);
		void unregisterListener(Listener*);
		void send(std::shared_ptr<Message>) const;
		bool isRegistered(Listener*) const;
	};
}

#endif // !INCLUDE_MESSSAGESENDER
