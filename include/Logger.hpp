#ifndef INCLUDE_LOGGER_HPP
#define INCLUDE_LOGGER_HPP

#include <fstream>
#include <string>
#include <sstream>

namespace breakout
{
	class Logger
	{
		std::fstream logfile;
		bool fileLoaded{ false };

		std::string getTimePrefix();
		void writeToLog(std::stringstream& const);
	public:
		Logger();
		~Logger();
		void finest(std::string);
		void fine(std::string);
		void info(std::string);
		void warning(std::string);
		void error(std::string);
	};
}

#endif // !INCLUDE_LOGGER_HPP
