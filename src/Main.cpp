#include <SFML/Graphics.hpp>

int main(int argc, char** argv)
{
	sf::CircleShape shape{ 250.f };
	shape.setFillColor(sf::Color::Blue);
	sf::RenderWindow window{ sf::VideoMode{500, 500}, "Breakout" };

	while (window.isOpen())
	{
		sf::Event event;
		window.pollEvent(event);
		switch (event.type)
		{
		case sf::Event::Closed:
			window.close();
			break;
		}
		window.clear(sf::Color::White);
		window.draw(shape);
		window.display();
	}
	return 0;
}