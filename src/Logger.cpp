#define _CRT_SECURE_NO_WARNINGS
#include "../include/Logger.hpp"
#include <ctime>
#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#endif // _WIN32


namespace breakout
{
	std::string Logger::getTimePrefix()
	{
		char temp[20];
		std::time_t time = std::time(nullptr);
		std::tm* tm = std::localtime(&time);
		std::strftime(temp, 20, "%F %T", tm);
		return std::string(temp);
	}

	void Logger::writeToLog(std::stringstream & const stream)
	{
		logfile << stream.str();
		std::cout << stream.str();
	}

	Logger::Logger()
	{
		#ifdef _WIN32
			const int BUFFERSIZE = 256;
			TCHAR envvar[BUFFERSIZE] = " ";
			GetEnvironmentVariable("localappdata", envvar, sizeof(envvar));
			std::string temp = envvar;
			temp += "/jumprun";
			//create jumprun dir
			if (!CreateDirectory(temp.c_str(), nullptr))
			{
				auto error = GetLastError();
				if (error == ERROR_PATH_NOT_FOUND)
				{
					temp = envvar;
				}
			}
			temp += "/jumprun.log";
			logfile.open(temp, std::ios::out | std::ios::app);
		#else
			logfile.open("jumprun.log", std::ios::out | std::ios::app);
		#endif // _WIN32	
		if (logfile)
		{
			fileLoaded = true;
		}
	}

	Logger::~Logger()
	{
		if (fileLoaded)
		{
			logfile.close();
		}
	}

	void Logger::finest(std::string msg)
	{
		std::stringstream stream;
		stream << getTimePrefix() << " [FINEST] " << msg;
		writeToLog(stream);
	}

	void Logger::fine(std::string msg)
	{
		std::stringstream stream;
		stream << getTimePrefix() << " [FINE] " << msg;
		writeToLog(stream);
	}

	void Logger::info(std::string msg)
	{
		std::stringstream stream;
		stream << getTimePrefix() << " [INFO] " << msg;
		writeToLog(stream);
	}

	void Logger::warning(std::string msg)
	{
		std::stringstream stream;
		stream << getTimePrefix() << " [WARNING] " << msg;
		writeToLog(stream);
	}

	void Logger::error(std::string msg)
	{
		std::stringstream stream;
		stream << getTimePrefix() << " [ERROR] " << msg;
		writeToLog(stream);
	}
}